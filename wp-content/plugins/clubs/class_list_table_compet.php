<?php
if(! class_exists('WP_List_Table')){
    require_once (ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}
require_once plugin_dir_path(__FILE__).'/bdd.php';

class class_list_table_compet extends WP_List_Table
{
    private $inscription;

    public function __construct() // CONSTRUCTEUR
    {
        parent::__construct([
            'singular' => __('Member', 'sp'),
            'plural' => __('Members', 'sp'),
        ]);
        $this->inscription = new bdd();
    }

    /**
     * préparation de la table
     */
    public function prepare_items($col="1")
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $this->process_bulk_action();

        $perPage = $this->get_items_per_page('nom_per_page', );
        $currentPage = $this->get_pagenum();
        $data = $this->inscription->findAll('challenges', $col);

/*      usort($data, array(&$this, 'usort_reorder'));
 */    

        $this->set_pagination_args([
            'total_items' => $totalPage,
            'per_page' => $perPage,
        ]);
        $paginateData = array_slice($data, (($currentPage -1) * $perPage), $perPage);
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $paginateData;


    }

    public function get_columns()
    {
        $columns = [
            'cb' => '<input type="checkbox"/>',
            'score' => 'Points',
            'id_competition' => 'Compétition',
            'id_adherent' => 'Adhérent'
        ];
        return $columns;
    }

    public function get_hidden_columns()
    {
        return array();
    }

    public function get_sortable_columns()
    {
        return $sortable = array(
            'score' => array('score',false),
            'id_adherent' => array('adherent',false),
            'id_competition' => array('position',false)
        );

    }

    public function column_default($item, $column_name)
    {
        switch ( $column_name ){
            case 'id_adherent':
                return $item[$column_name];
            case 'id_competition':
                if ($item[$column_name] == 1 ){
                    return 'Automobile';
                }else if ($item[$column_name] == 3){
                    return 'Aérien 3 rotors';
                }else if($item[$column_name] == 4){
                    return 'Aérien 4 rotors';
                }
            case 'score':
                return $item[ $column_name ];
            default:
                return print_r( $item, true );
        }
    }

    /**
     * function de trie
     * @param $a
     * @param $b
     * @return int|lt
     */
    function usort_reorder( $a, $b) {
        // If no sort, default to title
        $orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'score';
        // If no order, default to asc
        $order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
        // Determine sort order
        $result = strcmp( $a[$orderby], $b[$orderby] );
        // Send final sort direction to usort
        return ( $order === 'asc' ) ? $result : -$result;
    }

    /**
     * ajout des selecteurs d'actions
     * @return array
     */
    function get_bulk_actions()
    {
        $actions = [
            'delete' => 'Supprimer',
        ];
        return $actions;
    }

    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="id[]" value="%s"  />',
            $item['id']);
    }

    function process_bulk_action()
    {

        if('delete' === $this->current_action()){
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if(!empty($ids)){
                $this->inscription->deleteById($ids);
            }
        }
    }

    /**
     * action sur le nom
     *
     */
    function column_nom($item)
    {
        $actions = [
            'delete' => sprintf('<a href="?page=%s&action=%s&id=%s">Supprimer</a>', $_REQUEST['page'],'delete',$item['id']),
        ];
        return sprintf('%1$s %2$s', $item['nom'], $this->row_actions($actions));
    }
}
