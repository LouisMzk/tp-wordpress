<?php
/*
 * Plugin name: Ajout de clubs
 * Description: Permet d'enregitrer les différents clubs de la région en précisant quelques informations utiles
 * Author: Louis Mzk
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__).'/bdd.php';
require_once plugin_dir_path(__FILE__).'/class_list_table_member.php';
require_once plugin_dir_path(__FILE__).'/class_list_table_compet.php';
require_once plugin_dir_path(__FILE__).'/class_wp_list_classement.php';

 class clubs 
 {


     public function __construct()
     {
        register_activation_hook(__FILE__, array('bdd','create'));
        add_action('admin_menu', array($this,'add_menu_back') );
        add_action('admin_enqueue_scripts', array($this, 'linkscript') );
        add_action('admin_enqueue_scripts', array($this, 'linkstyle'));
        add_action('widgets_init', function(){
            register_widget('class_wp_list_classement');
        });

     }


    public function linkscript()
    {
        wp_register_script('script_admin', plugins_url('script_louis.js', __FILE__),'',null, true); 
        wp_enqueue_script('script_admin');
    }

    public function linkstyle(){
        wp_enqueue_style('admin-styles', get_template_directory_uri().'/style.css');
    }

     public function add_menu_back()
     {
         add_menu_page("Les clubs inscrits",
             'Les clubs', 'manage_options',
             'list',array($this,'myMenuBack'),
             'dashicons-format-aside',46);
 
         add_submenu_page('list', 'Liste des clubs',
             'Liste des clubs', 'manage_options',
             'list', array($this, 'myMenuBack'));

         add_submenu_page('list', 'Ajouter un club',
             'Ajouter un club', 'manage_options',
             'ajouter', array($this, 'myMenuBack'));
        
         add_menu_page("Compétition",
             'Compétition', 'manage_options',
             'update_compet',array($this,'myMenuBackCompet'),
             'dashicons-awards',46);

         add_submenu_page('update_compet', 'Enregistrer les résultats',
             'Résultats', 'manage_options',
             'update_compet', array($this, 'myMenuBackCompet'));
             
         add_submenu_page('update_compet', 'Classement',
             'Classement', 'manage_options',
             'classement', array($this, 'myMenuBackCompet'));

         add_menu_page("Statistiques", "Statistiques","manage_options",
            "stats",array($this, 'myMenuBack'),'dashicons-chart-bar', 46);


 
     }

     public function myMenuBack()
     {
 
         if($_GET[ 'page' ] == 'list' || isset($_POST['nom']) ) { // si on est sur une page membre
 
             echo "<h1>".get_admin_page_title()."</h1>";
             $ins = new bdd();
 
             if(isset($_POST['nom'])){
                 $ins->saveMember('clubs');
             }
 
             if((isset($_GET['action']) && $_GET['action'] == 'delete')){ // si on à cliquer sur un delete
                 $ins->deleteById($_GET['id']);
             }
 
             $table = new class_list_table_member();
             $table->prepare_items();
 
             echo $table->display();

 
         } 
          
         else if($_GET['page'] == 'ajouter'){
             echo "<h1>".get_admin_page_title()."</h1>";
 
             echo '<form action="" method="post">';
             echo '<p>';
             echo '<label for="nom">Nom du club :</label>';
             echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Email :</label>';
             echo '<input class="widefat" id="email" name="email" type="text" value="" />';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Adresse :</label>';
             echo '<input class="widefat" id="adresse" name="adresse" type="text" value="" />';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Téléphone :</label>';
             echo '<input class="widefat" id="tel" name="tel" type="text" value="" />';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Domaine : </label>';
             echo '<select name="domaine"><option value="automobile">Selectionnez</option><option value="automobile">Automobile</option><option value="naval">'.
                    'Naval</option><option value="aerien">Aérien</option></select>';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Compétitions : </label>';
             echo '<select name="participant"><option value=0>Selectionnez</option><option value=1>Inscrit</option><option value=0>Non inscrit</option></select>';
             echo '</p>';
             echo '<p><input type="submit" value="Ajouter ce club"></p>';
             echo '</form>';
         } else {
            // TODO: Calcul des stats
            echo 'Statistiques';
        }
 
 
     }
     
     public function myMenuBackCompet()
     {
 
         if($_GET[ 'page' ] == 'classement' || isset($_POST['num_adherent'])) { // si on est sur une page membre
 
             echo "<h1>".get_admin_page_title()."</h1>";
             echo '<form method="GET">';
             echo '<label>Catégorie de compétition : </label>';
             echo '<select name="domaine"><option value="1">Selectionnez</option><option value="1">Automobile</option>
             <option value="3">Aérien 3 rotors</option><option value="4">Aérien 4 rotors</option></select><input type="hidden" name="test" value="aerien"></input>
             <input type="hidden" name="page" value="classement"></input>';
             echo '&nbsp<input type="submit" name="cat" value="Ok"></input>';
             echo '</form>';

             $ins = new bdd();
 
             if(isset($_POST['num_adherent'])){
                 $ins->saveMember('challenges');
             }
 
             if((isset($_GET['action']) && $_GET['action'] == 'delete')){ // si on à cliquer sur un delete
                 $ins->deleteById($_GET['id']);
             }
 
             $table = new class_list_table_compet();
             $domaine = isset($_GET['domaine']) ? $_GET['domaine'] : 1;
             $table->prepare_items($domaine);
 
             echo $table->display();

 
         } 
         
         else {
             echo "<h1>".get_admin_page_title()."</h1>";
             echo '<form action="" method="post">';
             echo '<p>';
             echo '<label for="nom">Numero d\'adhérent :</label>';
             echo '<input class="widefat" id="num_adherent" name="num_adherent" type="text" value="" />';
             echo '</p>';
             echo '<p>';
             echo '<label for="nom">Position :</label>';
             echo '<input class="widefat" id="position" name="position" type="text" value="" />';
             echo '</p>';
             echo '<label for="nom">Domaine : </label>';
             echo '<select id="domaine" name="domaine"><option value="1">Selectionnez</option><option value="1">Automobile</option>
             <option value="aerien">Aérien</option></select>';
             echo '<label for="nom" class="invisible">&nbsp;Catégorie :</label>';
             echo '<select id="domaine-aerien" name="domaine_supp" class="invisible">
             <option value="3">3 rotors</option><option value="4">4 rotors</option></select>';             
             echo '<p><input type="submit" value="Enregistrer"></p>';
             echo '</form>';
         }
 
 
     }
 
 }

 new clubs();