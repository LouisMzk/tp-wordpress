<?php

require_once plugin_dir_path(__FILE__).'/class_list_table_compet.php';

class class_wp_list_classement extends WP_Widget{
    public function __construct()
    {
        $widget_opts = [
            'classname' => 'widget_list_classement',
            'description' => __('Widget classement'),
            'customize_selective_refresh' => true,
        ];

        parent::__construct('list_classement',__('Widget Classement','Classement'),$widget_opts);
    }

    public function form($instance)
    {

    }

    public function update($new_instance, $old_instance)
    {

    }

    public function widget($args, $instance)
    {
        echo "<table><td style='background:#111; color: #ddd'>Classement compétition</td><td>Automobile</td><td>Aérien 3 rotors</td><td>Aérien 4 rotors</td>
        <tr><td>Premier</td><td>Jeff (16 points)</td><td>Smith (150 points)</td><td>Roger (75 points)</td></tr><tr><td>Deuxième</td><td>Haley (10 points)</td><td>Bullock (100 points)</td><td>Homer (50 points)</td></tr>
        <tr><td>Troisième</td><td>Francine (8 points)</td><td>Steve (50 points)</td><td>Bart(25 points)</td></tr></table>";
    }
}