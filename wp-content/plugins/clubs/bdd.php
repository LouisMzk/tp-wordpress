<?php

class bdd
{
    public function __construct()
    {

    }

    public function create()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}clubs (id INT AUTO_INCREMENT PRIMARY KEY,".
            " nom VARCHAR (150) NOT NULL, email VARCHAR (255) NOT NULL, ".
            "adresse VARCHAR (255) NOT NULL, tel VARCHAR(100) NOT NULL, domaine VARCHAR(255) NOT NULL, participant INT NOT NULL);");
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}challenges (id INT AUTO_INCREMENT PRIMARY KEY,".
            " id_adherent VARCHAR (150) NOT NULL, id_competition INT (11) NOT NULL, ".
            "score INT (11));");
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}competitions (id INT AUTO_INCREMENT PRIMARY KEY,".
            " nom VARCHAR (150) NOT NULL);");
    }

    
    public function findAll($table, $col="")
    {
        global $wpdb;

        if($table == 'challenges'){
                $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}".$table." WHERE id_competition = ".$col." ORDER BY score desc;",
                    ARRAY_A);
        } else{
            $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}".$table.";",
            ARRAY_A);            
        }

        return $res;
    }

    public function deleteById($id)
    {
        if(!is_array($id)){
            $id = array($id);
        }
        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}clubs WHERE id in ("
            .implode(',',$id).");");
    }

    public function saveMember($table)
    {
        global $wpdb;

        if(isset($_POST['email']) && !empty($_POST['email'])){
            $nom = $_POST['nom'];
            $email = $_POST['email'];
            $adresse = $_POST['adresse'];
            $tel = $_POST['tel'];
            $domaine = $_POST['domaine'];
            $participant = (is_numeric($_POST['participant']))?($_POST['participant']):(0);

            $row = $wpdb->get_row("SELECT * FROM ".
                "{$wpdb->prefix}".$table." WHERE email='".
                $email."';");

            if(is_null($row)){ // si il n'ya pas d'enregistrement pour cet email
                $wpdb->insert("{$wpdb->prefix}clubs",
                    array(
                        'nom' => $nom, 
                        'email' => $email,
                        'adresse' => $adresse, 
                        'tel' => $tel,
                        'domaine' => $domaine,
                        'participant' => $participant
                    ));     
            }

        }

        if(isset($_POST["num_adherent"]) && !empty($_POST['num_adherent'])){
 
            $num_adherent = $_POST['num_adherent'];
            $position = $_POST['position'];
            // Si aerien est selectionné on va chercher la valeur du select de rotor
            $domaine = ($_POST['domaine']) == 'aerien' ? $_POST['domaine_supp'] : $_POST['domaine'];



            if($domaine == 1){
                if($position == 1){
                    $score = 8;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 1;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 1;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 1;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 2){
                    $score = 6;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 1;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 1;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 1;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 3){
                    $score = 4;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 1;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 1;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 1;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 4){
                    $score = 2;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 1;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 1;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 1;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 5){
                    $score = 1;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 1;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 1;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 1;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
            }


            if($domaine == 3){
                if($position == 1){
                    $score = 50;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 3;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 3;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 3;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 2){
                    $score = 25;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 3;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 3;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 3;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 3){
                    $score = 10;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 3;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 3;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 3;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 4){
                    $score = 5;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 3;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 3;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 3;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
            }


            if($domaine == 4){
                if($position == 1){
                    $score = 50;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 4;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 4;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 4;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 2){
                    $score = 25;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 4;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 4;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 4;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 3){
                    $score = 10;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 4;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 4;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 4;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
                if($position == 4){
                    $score = 5;
                    $row = $wpdb->get_row("SELECT * FROM ".
                    "{$wpdb->prefix}".$table." WHERE id_adherent='".
                    $num_adherent."' AND id_competition = 4;");

                    // Si le joueur a déjà des points
                    if($row >= 1){
                        $old_score = $wpdb->get_results("SELECT score FROM {$wpdb->prefix}".$table." WHERE id_adherent='".
                        $num_adherent."' AND id_competition = 4;",
                        ARRAY_A);
                       
                        $score += $old_score[0]['score'];
                        $wpdb->query("UPDATE {$wpdb->prefix}".$table." 
                         SET score = '".$score."'  
                        WHERE id_adherent=".$num_adherent." AND id_competition = 4;"); // Also works in this case.
                    } else{
                            $wpdb->insert("{$wpdb->prefix}challenges",
                            array(
                                'id_adherent' => (int)$num_adherent, 
                                'score' => $score,
                                'id_competition' => $domaine
                            )); 
                    }
                }
            }



        }
    }


    
}