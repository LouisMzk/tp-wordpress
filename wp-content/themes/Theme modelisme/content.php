<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class = "row">
        <?php if(has_post_thumbnail()) : ?>
            <div>
                <div class="thumbnail"><?php the_post_thumbnail('medium');?></div>
            </div>
            <div>
                <?php the_content() ?>
            </div>
        <?php else: ?>
            <div>
                <?php the_content() ?>
            </div>
        <?php endif; ?>

    </div>
</article>



