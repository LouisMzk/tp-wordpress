<?php
get_header();
?>

<main>
    <div class="container">
        <div class="row">
        <div>

            <?php
            if( have_posts() ) : while ( have_posts() ) : the_post();
                get_template_part('content', get_post_format());
            endwhile; endif;

            ?>
                <section>
                <div class="container">
                <?php if ( is_active_sidebar( 'competitions' ) ) : ?>
                <div class="widget-compet">
                <?php dynamic_sidebar( 'competitions' ); ?>
                </div>
                <?php endif; ?>
        </div>
    </section>
        </div>
    </div>
    </div>
</main>

<?php
get_footer();
