<html lang="<?php language_attributes(); ?>">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <?php  wp_head();?>
</head>
<body>
    <header>
        <div class="header-top" id="logo">
            <a href="/"><img src="<?php echo get_template_directory_uri().'/occitanie.png' ?>" alt="">
            <h1 class="text-center align-middle w-100">Fédération des clubs de modélisme d'Occitanie</h1></a>
        </div>
        <nav class="main-menu">
            <?php
                wp_nav_menu();
            ?>
        </nav>
    </header>

