<?php

function modelisme_setup()
{
    $args = array( // permet de créer un panel supplémentaire dans la personnalisation du theme pour changer l'image d'en tete
        'default-image'      => get_template_directory_uri() . '/images/Logo.jpg',
        'default-text-color' => '000',
        'width'              => '100',
        'height'             => '100',
        'flex-width'         => true,
        'flex-height'        => true,
    );
    add_theme_support( 'custom-header', $args );
}
add_action( 'after_setup_theme', 'modelisme_setup' );


function modelisme_theme_name_script()
{
    wp_register_style('main_style', get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');


    wp_register_style('bootstrap_style','https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(),true);
    wp_enqueue_script('bootstrap_script');
    
    wp_register_script('bootstrap_script','https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array(),true);
    wp_enqueue_style('bootstrap_style');
}

add_action('wp_enqueue_scripts', 'modelisme_theme_name_script');

function louis_widgets_init()
{
    if ( function_exists('register_sidebar') ) {

        register_sidebar(array(
            'name' => __('Widget competitions', 'w_modelisme'),
            'description' => __('Les widgets de compétitions', 'w_modelisme'),
            'id' => 'competitions',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5>',
            'after_title' => '</h5>',
        ));

    }
}

add_action('widgets_init','louis_widgets_init');



