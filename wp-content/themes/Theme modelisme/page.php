<?php
get_header();
?>

<main>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 blog-main">
                    <?php
                    if( have_posts() ) : while ( have_posts() ) : the_post();
                    get_template_part('content', get_post_format());
                    endwhile; endif;
                    ?>
                </div>
            </div>
        </div>
</section>
</main>

<?php
get_footer();
