=== Sirat ===
Contributors: VWthemes
Tags: left-sidebar, right-sidebar, one-column, two-columns, three-columns, four-columns, grid-layout, custom-colors, custom-background, custom-logo, custom-menu, custom-header, editor-style, featured-images, footer-widgets, sticky-post, full-width-template, theme-options, post-formats, translation-ready, threaded-comments, rtl-language-support, blog, portfolio, e-commerce
Requires at least: 4.8
Tested up to: 5.2.4
Requires PHP: 7.2.14
Stable tag: 0.5
License: GPLv3.0 or later
License URI: http://www.gnu.org/licenses/gpl-v3.0.html

Sirat is a multipurpose theme known primarily for its simplicity apart from being clean, user-friendly as well as finely organised making it a very good choice when it comes to WordPress themes relevant for various business purposes like a blog, portfolio, business website and WooCommerce storefront with a beautiful & professional design.

== Description ==

Sirat is a multipurpose theme known primarily for its simplicity apart from being clean, user-friendly as well as finely organised making it a very good choice when it comes to WordPress themes relevant for various business purposes like a blog, portfolio, business website and WooCommerce storefront with a beautiful & professional design. The theme comes with a new header layout and there are more options of sidebar layout available for post and pages. Apart from that, there are more width options as well as preload options available. Another important thing with this particular WordPress theme is that it is lightweight and can be extended to the limit one likes. It also has the availability of the enable and disable button. With some of its exemplary features like WooCommerce, retina ready, clean code, CTA, bootstrap framework, customization options, mobile friendliness etc, This WP theme has content limit option and is also accompanied with blog post settings.  Another important characteristic with it is the beauty it has and also the design that is immensely professional. Sirat is a fast theme and is accompanied with some of the best SEO practices in the online world. You have the option of customization a mentioned earlier making it extremely beneficial for personal portfolio. Apart from all this, Sirat is modern, luxurious, stunning as well as animated making it a fine option for the businesses where creativity is involved and it also has scroll top layout. Siart is translation ready and supports AR_ARABIC, DE_GERMAN, ES_SPANISH, FR_FRENCH, IT_ITALIAN, RU_RUSSIAN, ZH_CHINESE, TR_TURKISH languages. It fits creative business, small businesses restaurants, medical shops, startups, corporate businesses, online agencies and firms, portfolios, ecommerce. Sirat has banner opacity and colour changing feature included. 

== Changelog ==

= 0.1 =
* Initial version released.

= 0.2 =
	-- Updated language folder.
    -- Add footer layout option in customizer
    -- Add width layout option in customizer
    -- Add Show / hide Author, comment and post date option in customizer
    -- Add scroll to top with alignment option in customizer
    -- Add Global color option in customizer
    -- Add slider content layout option in customizer
    -- Add slider excerpt length option in customizer
    -- Add slider image opacity option in customizer
    -- Add logo resizer option in customizer

= 0.3 =
    -- Add header layout option in customizer
    -- Bug Fixes

= 0.4 =
    -- Add preloader option in customizer
    -- Add show / hide scroll to top option in customizer
    -- Update language folder

= 0.4.1 =
    -- Done the code for accessibility-ready.
    -- Bug fixes.

= 0.4.2 =
    -- Removed accessibility-ready tag.

= 0.4.3 =
    -- Resolved theme issue.

= 0.4.4 =
    -- Resolved the preloader issue.

= 0.4.5 =
    -- Added languages Po files.
    -- Done theme customization.
    -- Updated naviagtion menu code.

= 0.4.6 =
    -- Added show / hide sticky header option in customizer.

= 0.4.7 =
    -- Added blog layout option in customizer.
    -- Resolved css customization issues.

= 0.4.8 =
    -- Changed menu code.
    -- Done the css customization for single post page.
    -- Updated language folder.

= 0.4.9 =
    -- Changed single post and blog page layout.
    -- Changed responsive menu layout.
    -- Resolved the css customization.
    -- Added Show / hide topbar option in customizer.
    -- Added Show / hide woocommerce sidebar option in customizer.
    -- Updated language folder.

= 0.5 =
    -- Resolved the css customization.
    -- Added button text option in customizer.
    -- Resolved theme issues.

== Resources ==

Sirat WordPress Theme, Copyright 2019 VWthemes
Sirat is distributed under the terms of the GNU GPL.

Theme is Built using the following resource bundles.

* CSS bootstrap.css
-- Copyright 2011-2018 The Bootstrap Authors
-- https://github.com/twbs/bootstrap/blob/master/LICENSE
    
* JS bootstrap.js
-- Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
-- https://github.com/twbs/bootstrap/blob/master/LICENSE

* Free to use and abuse under the MIT license.
-- http://www.opensource.org/licenses/mit-license.php
-- font-awesome.css and fonts folder
Font Awesome 5.0.0 by @davegandy - http://fontawesome.io - @fontawesome
-- License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

* Customizer Pro, Copyright 2016 © Justin Tadlock.
License: All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
Source: https://github.com/justintadlock/trt-customizer-pro

* Theme Typography, Copyright 2016 © Justin Tadlock.
License: All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
Source: https://github.com/justintadlock/customizer-typography

* LucidScroll, Created by Shikkediel (c) 2013-2019 ataredo.com
License: All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
Source: http://ataredo.com/external/code/lucid.js

* Jquery, JS Foundation
License: Projects referencing this document are released under the terms of the MIT license.
Source: https://jquery.org/license/

* Stocksnap Images, 
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/license

Header Image, Copyright Christina Morillo
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/GVUWVKUHZE

About Image, Copyright Bruce Mars
License: CC0 1.0 Universal (CC0 1.0)
Source: https://stocksnap.io/photo/FCUDWWBKFV

* All the icons taken from genericons licensed under GPL License.
http://genericons.com/

== Theme Documentation ==
Documentation : https://www.vwthemesdemo.com/docs/free-sirat/