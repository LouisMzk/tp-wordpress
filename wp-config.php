<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'modelisme' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', '127.0.0.1' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'f2J0@e&,Wb-<Y;#xt]f|rYTtCK?1|h!>?0HMbd]%>tuKvcx+0T[*gV75Y>6F;~y|' );
define( 'SECURE_AUTH_KEY',  '(B/`) lvUQa6g|z,vGHQU5+bhb8tL$KqMxnJSw%gRKL=N<2HJsu>PBiO3!yq<-gr' );
define( 'LOGGED_IN_KEY',    '$rKp__eK86Dd)8fS<;u0<}!Oyr#Pagf**Of-c^[qDpI|(b$~sS!Bb<BE_fP^@`#}' );
define( 'NONCE_KEY',        'DYi2<R:g!|aI0cf5G?I[Tz=R/x`hdWQ5Cwd3#VZxGO.qf05e~TZgp}0Q~O/`XudD' );
define( 'AUTH_SALT',        '[ xpMR,q}[/2OIE??LwMex7(1dx>q%Oh``g/8n8a]H-uZ#Lw#!ziOeU!>P7x[b~A' );
define( 'SECURE_AUTH_SALT', 'YyN)IeXiiQ?|MMcA=>(S= #l0PnT>lBJXVk>UtWjgwcvlQ)|5*e5ux!aV;/>mcNc' );
define( 'LOGGED_IN_SALT',   'Xq1m7^$=z`le0<37.:ruj[@Hc-+5GP,;z`cG$hxBggfZk)M1[R6^t@ lOT/OHvBW' );
define( 'NONCE_SALT',       'hP1JtF^v#PEjSPpD`IM5(@>_GisQuIRF`pBbztM|;zw`T&%{WgANB-KQ5--g(|Fi' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
